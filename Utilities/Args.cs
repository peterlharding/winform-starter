﻿using System;
using System.Collections.Generic;
using System.IO;

//-------------------------------------------------------------------------------------------------

namespace Utilities
{
    //---------------------------------------------------------------------------------------------

    public class Args
    {
        public DateTime TimeStamp { get; set; }
        public int Threshold { get; set; }
        public int Id { get; set; }
        public string Component { get; set; }
        public DateTime TestDate { get; set; }
        public int Verbosity { get; set; }
        public bool ShowHelp { get; set; }
        public string SomeStringValue { get; set; }

        public IFormatProvider Culture { get; set; }

        public List<string> Extras;
        public List<string> Names;

        // public Dictionary<string, decimal> Rates { get; set; }

        //-----------------------------------------------------------------------------------------

        public Args(IEnumerable<string> args)
        {
            Threshold = 5;
            Names = new List<string>();
            Component = @"UNDEFINED";
            Id = 1;
            TestDate = DateTime.Now;
            TimeStamp = DateTime.Now;
            Verbosity = 0;

            var p = new OptionSet() {
                        { @"d|test_date=", "The date\n", (DateTime v) => TestDate = Convert.ToDateTime(v) },
                        { @"h|help",  "show this message and exit",  (bool v) => ShowHelp = v },
                        { @"i|id=", "The ID\n", (int v) => Id = v },
                        { @"t|threshold=", "threshold\n", (int v) => Threshold = v },
                        { @"c|component=", "Component\n", v => Component = v },
                        { @"n|name=", "the {NAME} of someone to greet.", v => Names.Add (v) },
                        { @"v", "increase debug message verbosity", v => { if (v != null) ++Verbosity; } },
                    };

            Culture = new System.Globalization.CultureInfo("en-AU", true);

            try
            {
                Extras = p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Write(@"Args: ");
                Console.WriteLine(e.Message);
                Console.WriteLine(@"Try `xxx --help' for more information.");
            }

            SomeStringValue = string.Join(" ", Extras.ToArray());

        } // Args

        //-----------------------------------------------------------------------------------------

        public override string ToString()
        {
            var sw = new StringWriter();

            sw.WriteLine($@"                           ID: [{Id}]");
            sw.WriteLine($@"                    Component: [{Component}]");
            sw.WriteLine($@"                        Names: [{Names}]");
            sw.WriteLine($@"                    Threshold: [{Threshold}]");
            sw.WriteLine($@"                         Date: [{TestDate:yyyy-MM-dd}]");
            sw.WriteLine($@"                    TimeStamp: [{TimeStamp:dd/MMM/yyyy H:mm:ss zzz}]");
            sw.WriteLine($@"                    Verbosity: [{Verbosity}]");
            sw.WriteLine();
            sw.WriteLine(@"                       Extras:");
            sw.WriteLine();

            foreach (var s in Extras)
            {
                sw.WriteLine($@"                           [{s}]");
            }

            sw.WriteLine();
            sw.WriteLine($@"Trailing options [{SomeStringValue}]");

            return sw.ToString();

        } // ToString

        //-----------------------------------------------------------------------------------------

    } // class Args

    //---------------------------------------------------------------------------------------------

} // namespace ScanUI

//-------------------------------------------------------------------------------------------------

