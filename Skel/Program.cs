﻿using System;
using System.IO;
using System.Windows.Forms;
using Skel.WinForms;
using Utilities;

namespace Skel
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(@"W:\");

            Logger.Init();
            Trace.Init();

            Logger.Log(@"About to decode Args");

            Logger.Log($@"[Program]  length args {args.Length}");

            Globals.Args = new Args(args);

            Logger.Log(@"Decoded Args");
            Logger.Log(Globals.Args.ToString());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
