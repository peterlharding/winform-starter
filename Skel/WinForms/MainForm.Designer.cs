﻿namespace Skel.WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Exit = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.Menu_File = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_New = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Close = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Separator_1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_File_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_File_RecentFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Separator_2 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_File_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Separator_1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Edit_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_View = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_View_One = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_View_Two = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_View_Three = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_View_Four = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Search = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools_ImageInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools_Something = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Tools_Options = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help_Separator_1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Help_About = new System.Windows.Forms.ToolStripMenuItem();
            this.Txt_Cmd = new System.Windows.Forms.TextBox();
            this.Rtb_Console = new System.Windows.Forms.RichTextBox();
            this.Lbl_Main_Version = new System.Windows.Forms.Label();
            this.Btn_DoSomething = new System.Windows.Forms.Button();
            this.Lbl_Command = new System.Windows.Forms.Label();
            this.Lbl_WorkFolder = new System.Windows.Forms.Label();
            this.Txt_WorkFolder = new System.Windows.Forms.TextBox();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Exit.Location = new System.Drawing.Point(826, 766);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Exit.TabIndex = 1;
            this.Btn_Exit.Text = "Exit";
            this.Btn_Exit.UseVisualStyleBackColor = true;
            this.Btn_Exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_File,
            this.Menu_Edit,
            this.Menu_View,
            this.Menu_Search,
            this.Menu_Tools,
            this.Menu_Help});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(913, 24);
            this.menuStrip.TabIndex = 18;
            this.menuStrip.Text = "menuStrip1";
            // 
            // Menu_File
            // 
            this.Menu_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_File_New,
            this.Menu_File_Open,
            this.Menu_File_Close,
            this.Menu_File_Separator_1,
            this.Menu_File_Save,
            this.Menu_File_SaveAs,
            this.toolStripSeparator2,
            this.Menu_File_RecentFiles,
            this.Menu_File_Separator_2,
            this.Menu_File_Exit});
            this.Menu_File.Name = "Menu_File";
            this.Menu_File.Size = new System.Drawing.Size(37, 20);
            this.Menu_File.Text = "File";
            this.Menu_File.Click += new System.EventHandler(this.Menu_File_New_Click);
            // 
            // Menu_File_New
            // 
            this.Menu_File_New.Name = "Menu_File_New";
            this.Menu_File_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.Menu_File_New.Size = new System.Drawing.Size(177, 22);
            this.Menu_File_New.Text = "New";
            // 
            // Menu_File_Open
            // 
            this.Menu_File_Open.Name = "Menu_File_Open";
            this.Menu_File_Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.Menu_File_Open.Size = new System.Drawing.Size(177, 22);
            this.Menu_File_Open.Text = "Open...";
            this.Menu_File_Open.Click += new System.EventHandler(this.Menu_File_Open_Click);
            // 
            // Menu_File_Close
            // 
            this.Menu_File_Close.Enabled = false;
            this.Menu_File_Close.Name = "Menu_File_Close";
            this.Menu_File_Close.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.Menu_File_Close.Size = new System.Drawing.Size(177, 22);
            this.Menu_File_Close.Text = "Close";
            this.Menu_File_Close.Click += new System.EventHandler(this.Menu_File_Close_Click);
            // 
            // Menu_File_Separator_1
            // 
            this.Menu_File_Separator_1.Name = "Menu_File_Separator_1";
            this.Menu_File_Separator_1.Size = new System.Drawing.Size(174, 6);
            // 
            // Menu_File_Save
            // 
            this.Menu_File_Save.Enabled = false;
            this.Menu_File_Save.Name = "Menu_File_Save";
            this.Menu_File_Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.Menu_File_Save.Size = new System.Drawing.Size(177, 22);
            this.Menu_File_Save.Text = "Save";
            this.Menu_File_Save.Click += new System.EventHandler(this.Menu_File_Save_Click);
            // 
            // Menu_File_SaveAs
            // 
            this.Menu_File_SaveAs.Enabled = false;
            this.Menu_File_SaveAs.Name = "Menu_File_SaveAs";
            this.Menu_File_SaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.Menu_File_SaveAs.Size = new System.Drawing.Size(177, 22);
            this.Menu_File_SaveAs.Text = "Save As";
            this.Menu_File_SaveAs.Click += new System.EventHandler(this.Menu_File_SaveAs_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(174, 6);
            // 
            // Menu_File_RecentFiles
            // 
            this.Menu_File_RecentFiles.Name = "Menu_File_RecentFiles";
            this.Menu_File_RecentFiles.Size = new System.Drawing.Size(177, 22);
            this.Menu_File_RecentFiles.Text = "Recent Files";
            // 
            // Menu_File_Separator_2
            // 
            this.Menu_File_Separator_2.Name = "Menu_File_Separator_2";
            this.Menu_File_Separator_2.Size = new System.Drawing.Size(174, 6);
            // 
            // Menu_File_Exit
            // 
            this.Menu_File_Exit.Name = "Menu_File_Exit";
            this.Menu_File_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.Menu_File_Exit.Size = new System.Drawing.Size(177, 22);
            this.Menu_File_Exit.Text = "Exit";
            this.Menu_File_Exit.Click += new System.EventHandler(this.Menu_File_Exit_Click);
            // 
            // Menu_Edit
            // 
            this.Menu_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Edit_Copy,
            this.Menu_Edit_Cut,
            this.Menu_Edit_Paste,
            this.Menu_Edit_Separator_1,
            this.Menu_Edit_Delete});
            this.Menu_Edit.Name = "Menu_Edit";
            this.Menu_Edit.Size = new System.Drawing.Size(39, 20);
            this.Menu_Edit.Text = "Edit";
            // 
            // Menu_Edit_Copy
            // 
            this.Menu_Edit_Copy.Enabled = false;
            this.Menu_Edit_Copy.Name = "Menu_Edit_Copy";
            this.Menu_Edit_Copy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.Menu_Edit_Copy.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Copy.Text = "Copy";
            this.Menu_Edit_Copy.Click += new System.EventHandler(this.Menu_Edit_Copy_Click);
            // 
            // Menu_Edit_Cut
            // 
            this.Menu_Edit_Cut.Enabled = false;
            this.Menu_Edit_Cut.Name = "Menu_Edit_Cut";
            this.Menu_Edit_Cut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.Menu_Edit_Cut.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Cut.Text = "Cut";
            this.Menu_Edit_Cut.Click += new System.EventHandler(this.Menu_Edit_Cut_Click);
            // 
            // Menu_Edit_Paste
            // 
            this.Menu_Edit_Paste.Enabled = false;
            this.Menu_Edit_Paste.Name = "Menu_Edit_Paste";
            this.Menu_Edit_Paste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.Menu_Edit_Paste.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Paste.Text = "Paste";
            this.Menu_Edit_Paste.Click += new System.EventHandler(this.Menu_Edit_Paste_Click);
            // 
            // Menu_Edit_Separator_1
            // 
            this.Menu_Edit_Separator_1.Name = "Menu_Edit_Separator_1";
            this.Menu_Edit_Separator_1.Size = new System.Drawing.Size(141, 6);
            // 
            // Menu_Edit_Delete
            // 
            this.Menu_Edit_Delete.Enabled = false;
            this.Menu_Edit_Delete.Name = "Menu_Edit_Delete";
            this.Menu_Edit_Delete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.Menu_Edit_Delete.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Delete.Text = "Delete";
            this.Menu_Edit_Delete.Click += new System.EventHandler(this.Menu_Edit_Delete_Click);
            // 
            // Menu_View
            // 
            this.Menu_View.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_View_One,
            this.Menu_View_Two,
            this.Menu_View_Three,
            this.toolStripSeparator1,
            this.Menu_View_Four});
            this.Menu_View.Name = "Menu_View";
            this.Menu_View.Size = new System.Drawing.Size(44, 20);
            this.Menu_View.Text = "View";
            // 
            // Menu_View_One
            // 
            this.Menu_View_One.Name = "Menu_View_One";
            this.Menu_View_One.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D1)));
            this.Menu_View_One.Size = new System.Drawing.Size(168, 22);
            this.Menu_View_One.Text = "One...";
            this.Menu_View_One.Click += new System.EventHandler(this.Menu_View_One_Click);
            // 
            // Menu_View_Two
            // 
            this.Menu_View_Two.Name = "Menu_View_Two";
            this.Menu_View_Two.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D2)));
            this.Menu_View_Two.Size = new System.Drawing.Size(168, 22);
            this.Menu_View_Two.Text = "Two";
            // 
            // Menu_View_Three
            // 
            this.Menu_View_Three.Name = "Menu_View_Three";
            this.Menu_View_Three.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D3)));
            this.Menu_View_Three.Size = new System.Drawing.Size(168, 22);
            this.Menu_View_Three.Text = "Three";
            this.Menu_View_Three.Click += new System.EventHandler(this.Menu_View_Three_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(165, 6);
            // 
            // Menu_View_Four
            // 
            this.Menu_View_Four.Name = "Menu_View_Four";
            this.Menu_View_Four.ShortcutKeyDisplayString = "";
            this.Menu_View_Four.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D4)));
            this.Menu_View_Four.Size = new System.Drawing.Size(168, 22);
            this.Menu_View_Four.Text = "Four";
            this.Menu_View_Four.Click += new System.EventHandler(this.Menu_View_Four_Click);
            // 
            // Menu_Search
            // 
            this.Menu_Search.Name = "Menu_Search";
            this.Menu_Search.Size = new System.Drawing.Size(54, 20);
            this.Menu_Search.Text = "Search";
            // 
            // Menu_Tools
            // 
            this.Menu_Tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Tools_ImageInfo,
            this.Menu_Tools_Something,
            this.Menu_Tools_Separator1,
            this.Menu_Tools_Options});
            this.Menu_Tools.Name = "Menu_Tools";
            this.Menu_Tools.Size = new System.Drawing.Size(47, 20);
            this.Menu_Tools.Text = "Tools";
            // 
            // Menu_Tools_ImageInfo
            // 
            this.Menu_Tools_ImageInfo.Name = "Menu_Tools_ImageInfo";
            this.Menu_Tools_ImageInfo.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.I)));
            this.Menu_Tools_ImageInfo.Size = new System.Drawing.Size(227, 22);
            this.Menu_Tools_ImageInfo.Text = "Image Info...";
            this.Menu_Tools_ImageInfo.Click += new System.EventHandler(this.Menu_Tools_ImageInfo_Click);
            // 
            // Menu_Tools_Something
            // 
            this.Menu_Tools_Something.Name = "Menu_Tools_Something";
            this.Menu_Tools_Something.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.Menu_Tools_Something.Size = new System.Drawing.Size(227, 22);
            this.Menu_Tools_Something.Text = "Do Something...";
            this.Menu_Tools_Something.Click += new System.EventHandler(this.Menu_Tools_Something_Click);
            // 
            // Menu_Tools_Separator1
            // 
            this.Menu_Tools_Separator1.Name = "Menu_Tools_Separator1";
            this.Menu_Tools_Separator1.Size = new System.Drawing.Size(224, 6);
            // 
            // Menu_Tools_Options
            // 
            this.Menu_Tools_Options.Name = "Menu_Tools_Options";
            this.Menu_Tools_Options.Size = new System.Drawing.Size(227, 22);
            this.Menu_Tools_Options.Text = "Options";
            this.Menu_Tools_Options.Click += new System.EventHandler(this.Menu_Tools_Options_Click);
            // 
            // Menu_Help
            // 
            this.Menu_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Help_Help,
            this.Menu_Help_Separator_1,
            this.Menu_Help_About});
            this.Menu_Help.Name = "Menu_Help";
            this.Menu_Help.Size = new System.Drawing.Size(44, 20);
            this.Menu_Help.Text = "Help";
            // 
            // Menu_Help_Help
            // 
            this.Menu_Help_Help.Name = "Menu_Help_Help";
            this.Menu_Help_Help.Size = new System.Drawing.Size(107, 22);
            this.Menu_Help_Help.Text = "Help";
            this.Menu_Help_Help.Click += new System.EventHandler(this.Menu_Help_Help_Click);
            // 
            // Menu_Help_Separator_1
            // 
            this.Menu_Help_Separator_1.Name = "Menu_Help_Separator_1";
            this.Menu_Help_Separator_1.Size = new System.Drawing.Size(104, 6);
            // 
            // Menu_Help_About
            // 
            this.Menu_Help_About.Name = "Menu_Help_About";
            this.Menu_Help_About.Size = new System.Drawing.Size(107, 22);
            this.Menu_Help_About.Text = "About";
            this.Menu_Help_About.Click += new System.EventHandler(this.Menu_Help_About_Click);
            // 
            // Txt_Cmd
            // 
            this.Txt_Cmd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Cmd.Location = new System.Drawing.Point(72, 27);
            this.Txt_Cmd.Name = "Txt_Cmd";
            this.Txt_Cmd.ReadOnly = true;
            this.Txt_Cmd.Size = new System.Drawing.Size(325, 20);
            this.Txt_Cmd.TabIndex = 2;
            this.Txt_Cmd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Txt_Cmd_KeyUp);
            // 
            // Rtb_Console
            // 
            this.Rtb_Console.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Rtb_Console.CausesValidation = false;
            this.Rtb_Console.Location = new System.Drawing.Point(12, 53);
            this.Rtb_Console.Name = "Rtb_Console";
            this.Rtb_Console.ReadOnly = true;
            this.Rtb_Console.Size = new System.Drawing.Size(889, 707);
            this.Rtb_Console.TabIndex = 20;
            this.Rtb_Console.TabStop = false;
            this.Rtb_Console.Text = "";
            // 
            // Lbl_Main_Version
            // 
            this.Lbl_Main_Version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl_Main_Version.AutoSize = true;
            this.Lbl_Main_Version.BackColor = System.Drawing.SystemColors.Window;
            this.Lbl_Main_Version.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Main_Version.Location = new System.Drawing.Point(815, 9);
            this.Lbl_Main_Version.Name = "Lbl_Main_Version";
            this.Lbl_Main_Version.Size = new System.Drawing.Size(86, 13);
            this.Lbl_Main_Version.TabIndex = 23;
            this.Lbl_Main_Version.Text = "Version N.N.N.N";
            this.Lbl_Main_Version.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Btn_DoSomething
            // 
            this.Btn_DoSomething.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_DoSomething.Location = new System.Drawing.Point(745, 766);
            this.Btn_DoSomething.Name = "Btn_DoSomething";
            this.Btn_DoSomething.Size = new System.Drawing.Size(75, 23);
            this.Btn_DoSomething.TabIndex = 24;
            this.Btn_DoSomething.Text = "Do It";
            this.Btn_DoSomething.UseVisualStyleBackColor = true;
            this.Btn_DoSomething.Click += new System.EventHandler(this.Btn_DoSomething_Click);
            // 
            // Lbl_Command
            // 
            this.Lbl_Command.AutoSize = true;
            this.Lbl_Command.Location = new System.Drawing.Point(12, 30);
            this.Lbl_Command.Name = "Lbl_Command";
            this.Lbl_Command.Size = new System.Drawing.Size(54, 13);
            this.Lbl_Command.TabIndex = 25;
            this.Lbl_Command.Text = "Command";
            // 
            // Lbl_WorkFolder
            // 
            this.Lbl_WorkFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl_WorkFolder.AutoSize = true;
            this.Lbl_WorkFolder.Location = new System.Drawing.Point(614, 30);
            this.Lbl_WorkFolder.Name = "Lbl_WorkFolder";
            this.Lbl_WorkFolder.Size = new System.Drawing.Size(62, 13);
            this.Lbl_WorkFolder.TabIndex = 26;
            this.Lbl_WorkFolder.Text = "WorkFolder";
            // 
            // Txt_WorkFolder
            // 
            this.Txt_WorkFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_WorkFolder.Location = new System.Drawing.Point(682, 27);
            this.Txt_WorkFolder.Name = "Txt_WorkFolder";
            this.Txt_WorkFolder.ReadOnly = true;
            this.Txt_WorkFolder.Size = new System.Drawing.Size(219, 20);
            this.Txt_WorkFolder.TabIndex = 27;
            this.Txt_WorkFolder.Click += new System.EventHandler(this.Txt_WorkFolder_Clicked);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 801);
            this.Controls.Add(this.Txt_WorkFolder);
            this.Controls.Add(this.Lbl_WorkFolder);
            this.Controls.Add(this.Lbl_Command);
            this.Controls.Add(this.Btn_DoSomething);
            this.Controls.Add(this.Lbl_Main_Version);
            this.Controls.Add(this.Rtb_Console);
            this.Controls.Add(this.Txt_Cmd);
            this.Controls.Add(this.Btn_Exit);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "Skeleton C# WinForms Program";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Exit;
		private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem Menu_File;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Open;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Close;
        private System.Windows.Forms.ToolStripSeparator Menu_File_Separator_1;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Save;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_SaveAs;
        private System.Windows.Forms.ToolStripSeparator Menu_File_Separator_2;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Exit;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Copy;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Cut;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Paste;
        private System.Windows.Forms.ToolStripSeparator Menu_Edit_Separator_1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Delete;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help_Help;
        private System.Windows.Forms.ToolStripSeparator Menu_Help_Separator_1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help_About;
        private System.Windows.Forms.TextBox Txt_Cmd;
        private System.Windows.Forms.RichTextBox Rtb_Console;
        private System.Windows.Forms.Label Lbl_Main_Version;
        private System.Windows.Forms.Button Btn_DoSomething;
        private System.Windows.Forms.Label Lbl_Command;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools_Something;
        private System.Windows.Forms.ToolStripSeparator Menu_Tools_Separator1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools_Options;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools_ImageInfo;
        private System.Windows.Forms.ToolStripMenuItem Menu_View;
        private System.Windows.Forms.ToolStripMenuItem Menu_View_One;
        private System.Windows.Forms.ToolStripMenuItem Menu_View_Three;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem Menu_View_Four;
        private System.Windows.Forms.ToolStripMenuItem Menu_View_Two;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_New;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_RecentFiles;
        private System.Windows.Forms.Label Lbl_WorkFolder;
        private System.Windows.Forms.TextBox Txt_WorkFolder;
    }
}

