﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using Utilities;

namespace Skel.WinForms
{
    public partial class MainForm : Form
    {
        //-----------------------------------------------------------------------------------------

        internal string ImageFolder { get; } = @"C:\Temp";
        internal string ImageFilePath { get; set; }

        internal string WorkFolder { get; }

        internal string MachineName { get; set; }
        internal string UserDisplayName { get; set; }
        internal string LocalUsername { get; set; }
        internal string NetworkUsername { get; set; }
        internal string UserDetails { get; set; }

        internal bool Loaded { get; set; }
        internal bool Copied { get; set; }

        //-----------------------------------------------------------------------------------------
        
        public MainForm()
        {
            InitializeComponent();




        } // MainForm



        //=========================================================================================
        // Form Methods
        //-----------------------------------------------------------------------------------------

        #region - Form methods

        //-----------------------------------------------------------------------------------------

        private void MainForm_Load(object sender, EventArgs e)
        {
            RtbConsole.Setup(Rtb_Console);

            GetUserDetails();

            RtbConsole.WriteLine(@"[MainForm]  Initialization completed");
            RtbConsole.WriteLine($@"[MainForm]       Machine Name: [{MachineName}]");
            RtbConsole.WriteLine($@"[MainForm]  User Display Name: [{UserDisplayName}]");
            RtbConsole.WriteLine($@"[MainForm]     Local Username: [{LocalUsername}]");
            RtbConsole.WriteLine($@"[MainForm]   Network Username: [{NetworkUsername}]");

            RtbConsole.WriteLine();
            RtbConsole.WriteLine(Globals.Args.ToString());


            Lbl_Main_Version.Text = $@"Version {Application.ProductVersion}";

            Loaded = true;

        } // MainForm_Load

        //-----------------------------------------------------------------------------------------

        internal void KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (!Loaded) return;

            RtbConsole.WriteLine($@"e.KeyChar [{Convert.ToInt32(e.KeyChar):000}]");

            if ((e.KeyChar == 13) || (e.KeyChar == '\t'))
            {
                // DialogResult = DialogResult.OK;

                // SplitSpecifiedTestNumbers();

                // Close();
            }

        } // KeyPressed

        //-----------------------------------------------------------------------------------------

        internal void Txt_Cmd_KeyUp(object sender, KeyEventArgs e)
        {
            if (!Loaded) return;

            RtbConsole.WriteLine($@"e.KeyValue [{Convert.ToInt32(e.KeyValue):000}]");

            if ((e.KeyValue == 13) || (e.KeyValue == '\t'))
            {
                var cmd = Txt_Cmd.Text;

                Console.WriteLine(@"cmd [{0}]", cmd);

                Txt_Cmd.Clear();

                // DialogResult = DialogResult.OK;

                // SplitSpecifiedTestNumbers();

                // Close();
            }

        } // Txt_Cmd_KeyUp

        //-----------------------------------------------------------------------------------------

        private void Txt_WorkFolder_Clicked(object sender, EventArgs e)
        {
            SelectWorkFolder();

        } // Txt_WorkFolder_Clicked

        //-----------------------------------------------------------------------------------------

        #endregion - Form methods

        //=========================================================================================
        // Button Methods
        //-----------------------------------------------------------------------------------------

        #region - Button Methods

        //-----------------------------------------------------------------------------------------

        internal void Btn_DoSomething_Click(object sender, EventArgs e)
        {
            DoSomething();

        } // Btn_DoSomething_Click

        //-----------------------------------------------------------------------------------------

        internal void Btn_Exit_Click(object sender, EventArgs e)
        {
            Exit();

        } // Btn_Exit_Click

        //-----------------------------------------------------------------------------------------

        #endregion - Button Methods

        //=========================================================================================
        // Menu Methods
        //-----------------------------------------------------------------------------------------

        #region - Menu Methods

        //-----------------------------------------------------------------------------------------
        // File Menu
        //-----------------------------------------------------------------------------------------

        internal void Menu_File_New_Click(object sender, EventArgs e)
        {
            Menu_File_Close.Enabled = true;

            NewFile();

        } // Menu_File_New_Click
        
       //-----------------------------------------------------------------------------------------
        
        internal void Menu_File_Open_Click(object sender, EventArgs e)
        {
            Menu_Edit_Copy.Enabled = true;
            Menu_File_Close.Enabled = true;

            OpenFile();

            // if (ImageFilePath != null) _tiffFile = SelectTiffFile(ImageFilePath);

        } // Menu_File_Open_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_File_Close_Click(object sender, EventArgs e)
        {
            Menu_Edit_Copy.Enabled = false;
            Menu_File_Close.Enabled = false;

            CloseFile();

        } // Menu_File_Close_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_File_Save_Click(object sender, EventArgs e)
        {
            SaveFile();

        } // Menu_File_Save_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_File_SaveAs_Click(object sender, EventArgs e)
        {
            SaveFileAs();

        } // Menu_File_SaveAs_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_File_Recent_Click(object sender, EventArgs e)
        {

        } // Menu_File_Recent_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_File_Exit_Click(object sender, EventArgs e)
        {
            Exit();

        } // Menu_File_Exit_Click

        //-----------------------------------------------------------------------------------------
        // Edit Menu
        //-----------------------------------------------------------------------------------------

        internal void Menu_Edit_Copy_Click(object sender, EventArgs e)
        {
            CopyClipboard();

        } // Menu_Edit_Copy_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_Edit_Cut_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Not implemented yet", @"Image Cut", MessageBoxButtons.OK, MessageBoxIcon.Information);

        } // Menu_Edit_Cut_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_Edit_Paste_Click(object sender, EventArgs e)
        {

        } // Menu_Edit_Paste_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_Edit_Delete_Click(object sender, EventArgs e)
        {

        } // Menu_Edit_Delete_Click

        //-----------------------------------------------------------------------------------------
        // View Menu
        //-----------------------------------------------------------------------------------------

        private void Menu_View_One_Click(object sender, EventArgs e)
        {
            NotImplementedYet();

        } // Menu_View_One_Click


        private void Menu_View_Two_Click(object sender, EventArgs e)
        {
            NotImplementedYet();

        } // Menu_View_Two_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_View_Three_Click(object sender, EventArgs e)
        {
            NotImplementedYet();

        } // Menu_View_Three_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_View_Four_Click(object sender, EventArgs e)
        {
            NotImplementedYet();

        } // Menu_View_Four_Click

        //-----------------------------------------------------------------------------------------
        // Tools Menu
        //-----------------------------------------------------------------------------------------

        internal void Menu_Tools_ImageInfo_Click(object sender, EventArgs e)
        {
            // var infoDialog = new InfoForm(_imageInfo);

            // infoDialog.ShowDialog();

            MessageBox.Show(@"Not implemented yet", @"Image Info", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        } // Menu_Tools_ImageInfo_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_Tools_Something_Click(object sender, EventArgs e)
        {
            DoSomething();

        } // Menu_Tools_Something_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_Tools_Options_Click(object sender, EventArgs e)
        {
            // SetOptions();

        } // Menu_Tools_Options_Click

        //-----------------------------------------------------------------------------------------
        // Help Menu
        //-----------------------------------------------------------------------------------------

        internal void Menu_Help_Help_Click(object sender, EventArgs e)
        {
            Help();

        } // Menu_Help_Help_Click

        //-----------------------------------------------------------------------------------------

        internal void Menu_Help_About_Click(object sender, EventArgs e)
        {
            About();

        } // Menu_Help_About_Click

        //-----------------------------------------------------------------------------------------

        #endregion - Menu Methods

		//=========================================================================================
        // Menu Handlers
        //-----------------------------------------------------------------------------------------

        internal void CopyClipboard()
        {
            // _clipped = true;

            Menu_Edit_Paste.Enabled = true;

        } // CopyClipboard

        //-----------------------------------------------------------------------------------------

        internal void PasteClipboard()
        {
            
        } // PasteClipboard

        //-----------------------------------------------------------------------------------------

        //=========================================================================================
        // Utility functions
        //-----------------------------------------------------------------------------------------

        #region - Utility Functions

        //-----------------------------------------------------------------------------------------

        internal void GetUserDetails()
        {
            MachineName = Environment.MachineName;

            UserDisplayName = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;

            LocalUsername = Environment.UserName;

            var identity = System.Security.Principal.WindowsIdentity.GetCurrent();

            NetworkUsername = identity.Name;

            UserDetails = NetworkUsername != null ? $@"{NetworkUsername}@{MachineName}" : $@"{LocalUsername}@{MachineName}";

        } // GetUserDetails

        //-----------------------------------------------------------------------------------------

        internal void NewFile()
        {

        } // NewFile

        //-----------------------------------------------------------------------------------------


        internal void OpenFile()
        {
            ImageFilePath = SelectImageFile(ImageFolder);

            if (ImageFilePath != null) // throw new ArgumentNullException("tifFile");
            {
                ImageFilePath = Path.GetDirectoryName(ImageFolder);

                Menu_File_Save.Enabled = true;
                Menu_File_Close.Enabled = true;
                Menu_File_SaveAs.Enabled = true;
            }

        } // OpenFile

        //-----------------------------------------------------------------------------------------

        internal void CloseFile()
        {
            Menu_File_Save.Enabled = false;
            Menu_File_Close.Enabled = false;
            Menu_File_SaveAs.Enabled = false;

        } // CloseFile

        //-----------------------------------------------------------------------------------------

        internal void SaveFile()
        {

        } // SaveFile

        //-----------------------------------------------------------------------------------------

        internal void SaveFileAs()
        {


        } // SaveFileAs

        //-----------------------------------------------------------------------------------------

        internal void CopyToClipboard()
        {
            Copied = true;

            Menu_Edit_Paste.Enabled = true;

        } // CopyToClipboard

        //-----------------------------------------------------------------------------------------

        internal void CutToClipboard()
        {
            Copied = true;

            Menu_Edit_Paste.Enabled = true;

        } // CutToClipboard

        //-----------------------------------------------------------------------------------------

        internal void PasteFromClipboard()
        {
            if (Copied)
            {
                //  pass for the moment...
            }

        } // PasteFromClipboard

        //-----------------------------------------------------------------------------------------

        internal void Exit()
        {
            Application.Exit();
        }

        //-----------------------------------------------------------------------------------------

        internal string SelectTiffFile(string initialFolder)
        {
            var dlgOpenReciprocityFile = new OpenFileDialog
            {
                RestoreDirectory = true,
                FilterIndex = 1,
                Filter = @"Text Files (*.tif)|*.tif|All Files (*.*)|*.*",
                InitialDirectory = initialFolder
            };

            if (dlgOpenReciprocityFile.ShowDialog() == DialogResult.Cancel)
            {
                dlgOpenReciprocityFile.Dispose();
            }
            else
            {
                try
                {
                    return dlgOpenReciprocityFile.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(@"Some exception - {0}", ex), @"File Click Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return null;

        } // SelectTiffFile

        //-----------------------------------------------------------------------------------------

        internal string SelectPdfFile(string initialFolder)
        {
            var dlgOpenReciprocityFile = new OpenFileDialog
            {
                FilterIndex = 1,
                Filter = @"PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*",
                InitialDirectory = initialFolder,
                RestoreDirectory = true
            };

            if (dlgOpenReciprocityFile.ShowDialog() == DialogResult.Cancel)
            {
                dlgOpenReciprocityFile.Dispose();
            }
            else
            {
                try
                {
                    return dlgOpenReciprocityFile.FileName;
                }
                catch (Exception ex)
                {
                    var msg = $@"Some exception - {ex}";

                    MessageBox.Show(msg, @"File Click Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return null;

        } // SelectPdfFile

        //-----------------------------------------------------------------------------------------

        internal static string SelectDbfFile()
        {
            var dlgOpen = new OpenFileDialog
            {
                RestoreDirectory = true,
                FilterIndex = 1,
                Filter = @"DBF Files (*.dbf)|*.dbf|DBF Files (*.DBF)|*.DBF|All Files (*.*)|*.*",
                InitialDirectory = @"W:\Results\2013"
            };

            if (dlgOpen.ShowDialog() == DialogResult.Cancel)
            {
                dlgOpen.Dispose();
            }
            else
            {
                try
                {
                    return dlgOpen.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show($@"Some exception - {ex}", @"File Open Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return null;

        } // SelectDbfFile

        //-----------------------------------------------------------------------------------------

        internal void SelectWorkFolder()
        {
            var folderBrowserDialog = new FolderBrowserDialog
            {
                Description = @"Select the directory that you want to use as the root directory.",
                ShowNewFolderButton = false,
                SelectedPath = WorkFolder
            };

            // folderBrowserDialog.RootFolder = Environment.SpecialFolder.Desktop;
            // folderBrowserDialog.StartPosition = FormStartPosition.Manual;
            
            var result = folderBrowserDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                Txt_WorkFolder.Text = folderBrowserDialog.SelectedPath;
            }

        } // SelectWorkFolder

        //-----------------------------------------------------------------------------------------

        #endregion - Utility Functions

        //=========================================================================================
        // Various Methods
        //-----------------------------------------------------------------------------------------

        #region - Actions

        //-----------------------------------------------------------------------------------------

        public void NotImplementedYet()
        {
            MessageBox.Show(@"Replace this with something useful...", @"Not Implemented Yet", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        } // NotImplementedYet

        //-----------------------------------------------------------------------------------------

        internal void DoSomething()
        {
            RtbConsole.WriteLine(@"Do something...");

        } // DoSomething

        //-----------------------------------------------------------------------------------------

        internal void Help()
        {
            RtbConsole.WriteLine(@"Help...");

        } // DoSomething

        //-----------------------------------------------------------------------------------------

        internal void About()
        {
            RtbConsole.WriteLine(@"About...");

        } // DoSomething

        //-----------------------------------------------------------------------------------------

        #endregion - Actions

        //=========================================================================================
        // Image Handling Methods
        //-----------------------------------------------------------------------------------------

        #region - Image Handling Methods

        //-----------------------------------------------------------------------------------------

        internal string SelectImageFile(string initialFolder)
        {
            var dialog = new OpenFileDialog
            {
                RestoreDirectory = true,
                FilterIndex = 1,
                Filter = @"All Files (*.*)|*.*|PNG Files (*.png)|*.png|GIF Files (*.gif)|*.gif|JPEG Files (*.jpg)|*.jpg|Tiff Files (*.tif)|*.tif",
                InitialDirectory = initialFolder
            };

            if (dialog.ShowDialog() == DialogResult.Cancel)
            {
                dialog.Dispose();
            }
            else
            {
                try
                {
                    return dialog.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(@"Some exception - {0}", ex), @"File Open Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return null;

        } // SelectImageFile

        //-----------------------------------------------------------------------------------------

        internal void OpenImage()
        {
            /*
                        _imageFilePath = SelectImageFile(_imageFolder);

                        if (_imageFilePath != null) // throw new ArgumentNullException("tifFile");
                        {
                            _imageFolder = Path.GetDirectoryName(_imageFilePath);

                            Stopwatch sw  = new Stopwatch();

                            sw.Start();

                            PB_Viewer.Image = _image = new Bitmap(_imageFilePath);

                            sw.Stop();

                            _loadTimeMilliseconds = sw.ElapsedMilliseconds;

                            _imageSize = _image.Size;

                            _imageBoundsRect = GetImageBounds();

                            GetImageRectangle();

                            Menu_File_Save.Enabled = true;
                            Menu_File_Close.Enabled = true;
                            Menu_File_SaveAs.Enabled = true;
                            // Menu_Tools_ImageInfo.Enabled = true;

                            _imageInfo = new ImageInfo(_image, _imageFilePath);

                            _imageInfo.LoadTime = _loadTimeMilliseconds;
                        }
            */
        } // OpenImage

        //-----------------------------------------------------------------------------------------

        internal void CloseImage()
        {
            // PB_Viewer.Image = null;

            Menu_File_Save.Enabled = false;
            Menu_File_Close.Enabled = false;
            Menu_File_SaveAs.Enabled = false;
            // Menu_Tools_ImageInfo.Enabled = false;

        } // CloseImage

        //-----------------------------------------------------------------------------------------

        #endregion - Image Handling Methods

        //=========================================================================================
        // External Actions
        //-----------------------------------------------------------------------------------------

        #region - External Actions

        //-----------------------------------------------------------------------------------------

        internal void Explore(string folderPath)
        {
            try
            {
                Process.Start(folderPath);  // Runs explorer on this folder
            }
            catch (DirectoryNotFoundException exception)
            {
                var msg = $"The specified directory [{folderPath}] does not exist. {exception}";

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"Directory Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Win32Exception exception)
            {
                var msg = $"Win32Exception [{folderPath}] does not exist. {exception}";

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        } // Explore

        //-----------------------------------------------------------------------------------------

        internal void RunExplorer(string folderPath)
        {
            const string explorerPath = @"C:\Windows\explorer.exe";

            // var software = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Wow6432Node");
            // var irfanView = software.OpenSubKey("IrfanView");
            // var shell = irfanView.OpenSubKey("shell").OpenSubKey("open").OpenSubKey("command");

            try
            {
                Process.Start(explorerPath, folderPath);
            }
            catch (DirectoryNotFoundException ex)
            {
                var msg = $"The specified program [{explorerPath}] does not exist. {ex}";

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"Program Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Win32Exception ex)
            {
                String msg = String.Format("Win32Exception:  Path [{0}] does not exist. {1}", folderPath, ex);

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        } // RunExplorer

        //-----------------------------------------------------------------------------------------

        internal void RunNotepad(string txtFile)
        {
            const string program = @"C:\Apps\Notepad++\notepad++.exe";

            try
            {
                Process.Start(program, txtFile);  // @"C:\Temp\XXX.txt"
            }
            catch (DirectoryNotFoundException ex)
            {
                String msg = String.Format("The specified program [{0}] does not exist. {1}", program, ex);

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"Program Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Win32Exception ex)
            {
                String msg = String.Format("Win32Exception [{0}] does not exist. {1}", txtFile, ex);

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        } // RunNotepad

        //-----------------------------------------------------------------------------------------

        internal void RunPdfViewer(string pdfFile)
        {
            var readerPath = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\AcroRd32.exe", null, null).ToString().Replace("\"", "");

            try
            {
                Process.Start(readerPath, pdfFile);
            }
            catch (DirectoryNotFoundException ex)
            {
                String msg = String.Format("The Acrobat Reader [{0}] does not exist. {1}", readerPath, ex);

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"Program Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Win32Exception ex)
            {
                String msg = String.Format("Win32Exception:  PDF File [{0}] does not exist. {1}", pdfFile, ex);

                Console.WriteLine(msg);

                MessageBox.Show(msg, @"IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        } // RunPdfViewer

        //-----------------------------------------------------------------------------------------

        internal void RunImageViewer(string imageFile)
        {
            // const string tiffViewerExePath = @"C:\Apps\IrfanView\i_view32.exe";

            // var software = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Wow6432Node");
            // var software = Registry.LocalMachine.OpenSubKey("HKEY_CURRENT_USER");
            // var irfanView = software.OpenSubKey("IrfanView");
            // var shell = irfanView.OpenSubKey("shell").OpenSubKey("open").OpenSubKey("command");
            // var irfanview = Registry.GetValue(@"HKEY_CURRENT_USER\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\MuiCache", "IrfanView", null);

            string irfanview = null;

            // 64 bit installation

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (var irfanviewOpenCommandKey = hklm.OpenSubKey(@"SOFTWARE\IrfanView\shell\open\command"))
                {
                    if (irfanviewOpenCommandKey != null)
                        irfanview = (string)irfanviewOpenCommandKey.GetValue(@"", null);
                }

            if (irfanview != null)
            {
                irfanview = irfanview.Replace("\"", "");

                if (File.Exists(irfanview))
                {
                    try
                    {
                        Process.Start(irfanview, imageFile);
                    }
                    catch (DirectoryNotFoundException ex)
                    {
                        String msg = String.Format("The specified program [{0}] does not exist. {1}", irfanview, ex);

                        Console.WriteLine(msg);

                        MessageBox.Show(msg, @"Program Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Win32Exception ex)
                    {
                        String msg = String.Format("Win32Exception:  PDF File [{0}] does not exist. {1}", imageFile, ex);

                        Console.WriteLine(msg);

                        MessageBox.Show(msg, @"IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                } 
            }

        } // RunImageViewer

        //-----------------------------------------------------------------------------------------

        #endregion - External Actions

        //=========================================================================================
        // Misc Methods
        //-----------------------------------------------------------------------------------------

        #region - Misc Methods

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        #endregion - Misc Methods


        //=========================================================================================
        // New Stuff
        //-----------------------------------------------------------------------------------------

        internal void Extra()
        {


        } // Extra


        //=========================================================================================

    }
}
